import os


def rename_photos(folder_path, start_number=1):
    files = os.listdir(folder_path)

    photo_extensions = [".png"]
    photos = [file for file in files if file.lower().endswith(tuple(photo_extensions))]

    if not photos:
        print("No photos in the folder")
        return

    photos.sort()

    for index, photo in enumerate(photos):
        old_path = os.path.join(folder_path, photo)
        new_name = f"{start_number + index}.png"
        new_path = os.path.join(folder_path, new_name)
        os.rename(old_path, new_path)
        print(f"Renamed: {photo} -> {new_name}")


if __name__ == "__main__":
    folder_path = "new_img"
    rename_photos(folder_path)
