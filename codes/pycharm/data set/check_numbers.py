import os

folder_path = "images/test"

file_list = os.listdir(folder_path)
file_name_dict = {}

for file_name in file_list:
    base_name, ext = os.path.splitext(file_name)
    if ext.lower() == ".png" or ext.lower() == ".xml":
        if base_name in file_name_dict:
            file_name_dict[base_name] += 1
        else:
            file_name_dict[base_name] = 1

for base_name, count in file_name_dict.items():
    if count == 1:
        print(base_name)
    else:
        print('all good')
