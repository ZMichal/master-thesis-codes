import cv2
import os

folder = 1
a = 20

save_path = f'images/{folder}'

list_videos = os.listdir(f'videos/{folder}')
print(list_videos)

running_count = 1

for i in range(len(list_videos)):
    path = f'videos/{folder}/' + list_videos[i]
    cap = cv2.VideoCapture(path)
    count = 1

    while cap.isOpened():
        ret, frame = cap.read()

        count = count + 1
        if ret:
            if count % a == 0:
                cv2.imwrite(save_path + '/' + str(running_count) + '.png', frame)
                running_count = running_count + 1
        else:
            cap.release()

print("saved " + str(running_count - 1) + " images")
