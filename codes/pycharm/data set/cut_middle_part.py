import cv2
import os

roi_width = 360
roi_height = 640

input_folder = '2_data_set'
output_folder = '2_data_set_out'

os.makedirs(output_folder, exist_ok=True)

interval = 30

running_count = 1

for video_file in os.listdir(input_folder):
    video_path = os.path.join(input_folder, video_file)

    cap = cv2.VideoCapture(video_path)
    count = 0

    while cap.isOpened():
        ret, frame = cap.read()

        count = count + 1
        if ret:
            if count % interval == 0:
                frame_height, frame_width, _ = frame.shape
                roi_x = int((frame_width - roi_width) / 2)
                roi_y = int((frame_height - roi_height) / 2)

                roi = frame[roi_y:roi_y + roi_height, roi_x:roi_x + roi_width]

                output_file = os.path.join(output_folder, f'{running_count}.png')
                cv2.imwrite(output_file, roi)
                running_count += 1
        else:
            cap.release()

print("Saved " + str(running_count - 1) + " images.")