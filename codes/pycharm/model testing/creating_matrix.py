import cv2
import numpy as np
import tensorflow as tf
import os
import glob
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix

class_labels = ['Left cartouche', 'Lighthouse', 'Church tower', 'Logo PG',
                'Medusa', 'Prow', 'Right cartouche', 'Steam engine']

model_path = 'tflite models/custom8.tflite'
interpreter = tf.lite.Interpreter(model_path=model_path)
interpreter.allocate_tensors()

input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()


def recognize_image(image):
    input_shape = input_details[0]['shape']
    input_image = cv2.resize(image, (input_shape[1], input_shape[2]))
    input_image = np.expand_dims(input_image, axis=0)
    input_image = input_image.astype(np.float32) / 255.0

    interpreter.set_tensor(input_details[0]['index'], input_image)
    interpreter.invoke()

    class_scores = interpreter.get_tensor(output_details[0]['index'])
    box_coordinates = interpreter.get_tensor(output_details[1]['index'])
    classes = interpreter.get_tensor(output_details[3]['index'])

    max_score_index = np.argmax(class_scores[0])
    max_score = class_scores[0, max_score_index]
    class_id = int(classes[0, max_score_index])
    label = class_labels[class_id]
    return label, max_score


test_folders_path = 'test_folders'
true_labels = []
predicted_labels = []

for folder_name in os.listdir(test_folders_path):
    folder_path = os.path.join(test_folders_path, folder_name)
    if os.path.isdir(folder_path):
        image_files = sorted(
            glob.glob(os.path.join(folder_path, '*.png')) + glob.glob(os.path.join(folder_path, '*.jpg')))
        for image_path in image_files:
            image = cv2.imread(image_path)
            object_name, confidence = recognize_image(image)
            true_labels.append(folder_name)
            predicted_labels.append(object_name)
    print(f"{folder_name} done")

cm = confusion_matrix(true_labels, predicted_labels, labels=class_labels)

plt.figure(figsize=(12, 10))
heatmap = sns.heatmap(cm, annot=True, fmt='d', cmap='Blues', xticklabels=class_labels, yticklabels=class_labels)
plt.title('Confusion Matrix')
plt.xlabel('Predicted')
plt.ylabel('True')

heatmap.set_xticklabels(heatmap.get_xticklabels(), rotation=20, ha='right')
heatmap.set_yticklabels(heatmap.get_yticklabels(), rotation=0, ha='right')

plt.show()
