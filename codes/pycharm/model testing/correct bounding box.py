import cv2
import numpy as np
import tensorflow as tf
import os
import glob
from xml.etree import ElementTree as ET

class_labels = ['Left cartouche', 'Lighthouse', 'Church tower', 'Logo PG',
                'Medusa', 'Prow', 'Right cartouche', 'Steam engine']

model_path = 'tflite models/custom8.tflite'
interpreter = tf.lite.Interpreter(model_path=model_path)
interpreter.allocate_tensors()

input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()


def recognize_and_visualize(image, xml_path):
    input_shape = input_details[0]['shape']
    input_image = cv2.resize(image, (input_shape[1], input_shape[2]))
    input_image = np.expand_dims(input_image, axis=0)
    input_image = input_image.astype(np.float32) / 255.0

    interpreter.set_tensor(input_details[0]['index'], input_image)
    interpreter.invoke()

    class_scores = interpreter.get_tensor(output_details[0]['index'])
    box_coordinates = interpreter.get_tensor(output_details[1]['index'])
    classes = interpreter.get_tensor(output_details[3]['index'])

    max_score_index = np.argmax(class_scores[0])
    max_score = class_scores[0, max_score_index]
    class_id = int(classes[0, max_score_index])
    label = class_labels[class_id]

    tree = ET.parse(xml_path)
    root = tree.getroot()
    object_data = root.find('object')
    xmin = int(object_data.find('bndbox/xmin').text)
    ymin = int(object_data.find('bndbox/ymin').text)
    xmax = int(object_data.find('bndbox/xmax').text)
    ymax = int(object_data.find('bndbox/ymax').text)

    color = (0, 255, 0)
    cv2.rectangle(image, (xmin, ymin), (xmax, ymax), color, 2)
    cv2.putText(image, f"True: {label}", (xmin, ymin - 10),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

    predicted_box = box_coordinates[0, max_score_index]
    xmin_pred = int(predicted_box[1] * image.shape[1])
    ymin_pred = int(predicted_box[0] * image.shape[0])
    xmax_pred = int(predicted_box[3] * image.shape[1])
    ymax_pred = int(predicted_box[2] * image.shape[0])
    color = (0, 0, 255)
    cv2.rectangle(image, (xmin_pred, ymin_pred), (xmax_pred, ymax_pred), color, 2)
    cv2.putText(image, f"Predicted: {label} {100 * max_score:.1f}%", (xmin_pred, ymin_pred - 10),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

    return label, max_score


test_folders_path = 'test_folders'

for folder_name in os.listdir(test_folders_path):
    folder_path = os.path.join(test_folders_path, folder_name)
    if os.path.isdir(folder_path):
        image_files = sorted(
            glob.glob(os.path.join(folder_path, '*.png')) + glob.glob(os.path.join(folder_path, '*.jpg')))
        correct_count = 0

        for image_path in image_files:
            image = cv2.imread(image_path)
            xml_path = os.path.join(folder_path, f"{os.path.splitext(os.path.basename(image_path))[0]}.xml")
            object_name, confidence = recognize_and_visualize(image, xml_path)

            if object_name == folder_name:
                correct_count += 1

            cv2.imshow('Detected Objects', image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        total_images = len(image_files)
        print(f"For class {folder_name}, {correct_count} out of {total_images} images were recognized correctly.")
