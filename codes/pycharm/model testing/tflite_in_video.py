import cv2
import numpy as np
import tensorflow as tf

model_path = 'tflite models/custom8.tflite'
interpreter = tf.lite.Interpreter(model_path=model_path)
interpreter.allocate_tensors()
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

class_labels = ['Left cartouche', 'Lighthouse', 'Church tower', 'Logo PG',
                'Medusa', 'Prow', 'Right cartouche', 'Steam engine']
object_count = 0
object_name = "Steam engine"


def recognize_image(frame):
    global object_count
    input_shape = input_details[0]['shape']
    input_image = cv2.resize(frame, (input_shape[1], input_shape[2]))
    input_image = np.expand_dims(input_image, axis=0)
    input_image = input_image.astype(np.float32) / 255.0

    interpreter.set_tensor(input_details[0]['index'], input_image)
    interpreter.invoke()

    output_data = [interpreter.get_tensor(output_detail['index']) for output_detail in output_details]

    boxes = output_data[1]
    classes = output_data[3]
    scores = output_data[0]

    for i in range(boxes.shape[1]):
        class_value = classes[0, i]
        score_value = scores[0, i]

        if score_value > 0.5:
            box = boxes[0, i]
            top, left, bottom, right = box * [frame.shape[0], frame.shape[1], frame.shape[0], frame.shape[1]]

            class_id = int(class_value)
            class_name = class_labels[class_id]
            percentage = int(score_value * 100)

            text = f"{class_name} {percentage}%"

            cv2.rectangle(frame, (int(left), int(top)), (int(right), int(bottom)), (0, 255, 0), 2)
            cv2.putText(frame, text, (int(left), int(top)), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 2)

            if class_name == object_name:
                object_count += 1
    return frame


video_path = 'videos/all/vid32.mp4'
cap = cv2.VideoCapture(video_path)
frame_count = 0
while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break
    frame_count += 1
    if frame.shape[0] > frame.shape[1]:
        new_width = int(frame.shape[1] * (720 / frame.shape[0]))
        new_height = 720
    else:
        new_width = 1280
        new_height = int(frame.shape[0] * (1080 / frame.shape[1]))

    resized_frame = cv2.resize(frame, (new_width, new_height))
    result_frame = recognize_image(resized_frame)
    cv2.imshow('Detected Objects', result_frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()

print(f"{object_name} was recognized in {object_count} out of {frame_count} frames.")
