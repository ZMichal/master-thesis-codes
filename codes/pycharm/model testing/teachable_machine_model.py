import tensorflow as tf
import cv2
import numpy as np
import os

model_path = "tflite models/model_quant.tflite"

interpreter = tf.lite.Interpreter(model_path=model_path)
interpreter.allocate_tensors()

input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

input_shape = input_details[0]['shape']

folder_path = "objects/R cartouche"

image_files = [f for f in os.listdir(folder_path) if f.endswith('.PNG') or f.endswith('.jpg') or f.endswith('.png')]

for image_file in image_files:
    image_path = os.path.join(folder_path, image_file)
    image = cv2.imread(image_path)
    image = cv2.resize(image, (input_shape[1], input_shape[2]))
    image = image.astype(np.uint8)
    image = np.expand_dims(image, axis=0)
    interpreter.set_tensor(input_details[0]['index'], image)
    interpreter.invoke()
    output_data = interpreter.get_tensor(output_details[0]['index'])
    predicted_class = np.argmax(output_data)
    print(f"Predicted class for {image_file}: {predicted_class}")
