import cv2
import numpy as np
import tensorflow as tf
import os
import glob

class_labels = ['Left cartouche', 'Lighthouse', 'Church tower', 'Logo PG',
                'Medusa', 'Prow', 'Right cartouche', 'Steam engine']

model_path = 'tflite models/custom8.tflite'
interpreter = tf.lite.Interpreter(model_path=model_path)
interpreter.allocate_tensors()

input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()


def recognize_image(image, object_name):
    max_dimension = 920
    height, width, _ = image.shape
    if height > max_dimension or width > max_dimension:
        if height > width:
            scale_factor = max_dimension / height
            new_height = max_dimension
            new_width = int(width * scale_factor)
        else:
            scale_factor = max_dimension / width
            new_width = max_dimension
            new_height = int(height * scale_factor)
        image = cv2.resize(image, (new_width, new_height))

    input_shape = input_details[0]['shape']
    input_image = cv2.resize(image, (input_shape[1], input_shape[2]))
    input_image = np.expand_dims(input_image, axis=0)
    input_image = input_image.astype(np.float32) / 255.0

    interpreter.set_tensor(input_details[0]['index'], input_image)
    interpreter.invoke()

    class_scores = interpreter.get_tensor(output_details[0]['index'])
    box_coordinates = interpreter.get_tensor(output_details[1]['index'])
    classes = interpreter.get_tensor(output_details[3]['index'])

    k, correct = 0, 0

    for i in range(len(class_scores[0])):
        if class_scores[0, i] > 0.5:
            k += 1
            ymin, xmin, ymax, xmax = box_coordinates[0, i]
            xmin = int(xmin * image.shape[1])
            xmax = int(xmax * image.shape[1])
            ymin = int(ymin * image.shape[0])
            ymax = int(ymax * image.shape[0])

            class_score = class_scores[0, i]
            class_id = int(classes[0, i])
            if 0 <= class_id < len(class_labels):
                label = class_labels[class_id]
            else:
                label = "Unknown"
            if label == object_name:
                correct = 1

            color = (0, 255, 0)
            cv2.rectangle(image, (xmin, ymin), (xmax, ymax), color, 2)
            cv2.putText(image, f"{label} {100 * class_score:.1f}%",
                        (xmin, ymin - 10), cv2.FONT_HERSHEY_SIMPLEX,
                        0.5, color, 2)
        if k == 0:
            cv2.putText(image, f"NOT DETECTED", (10, 30),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.5, (0, 0, 255), 2)
    cv2.imshow('Detected Objects', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return correct


folder_path = 'images/testowe'
image_files = sorted(glob.glob(os.path.join(folder_path, '*.png')) + glob.glob(os.path.join(folder_path, '*.jpg')))

recognized_count = 0
for image_path in image_files:
    image = cv2.imread(image_path)
    result_image = recognize_image(image, "Church tower")
    if result_image == 1:
        recognized_count += 1

total_images = len(image_files)
print(f"{recognized_count} out of {total_images} images were recognized correctly.")
