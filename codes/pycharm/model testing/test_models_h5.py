import os
import cv2
import numpy as np
from keras.models import load_model


def predict_single_image(model, image_path, class_names):
    original_image = cv2.imread(image_path)

    resized_image = cv2.resize(original_image, (360, 640))
    resized_image = resized_image.astype('float32') / 255.0
    resized_image = np.expand_dims(resized_image, axis=0)

    predictions = model.predict(resized_image)
    predicted_class = np.argmax(predictions)

    class_name = class_names[predicted_class]
    confidence_percent = predictions[0][predicted_class] * 100

    return class_name


def test_models_on_folder(model_paths, folder_path, class_names):
    correct_predictions = [0, 0, 0, 0, 0, 0, 0, 0]
    total_samples = 0

    folder_name = os.path.basename(folder_path)

    for image_file in os.listdir(folder_path):
        image_path = os.path.join(folder_path, image_file)

        if not os.path.isfile(image_path):
            continue

        total_samples += 1

        for i, model_path in enumerate(model_paths):
            model = load_model(model_path)
            predicted_class = predict_single_image(model, image_path, class_names)
            print(f"Model {i + 1} Predicted Class: {predicted_class}")

            if str(predicted_class) == folder_name:
                correct_predictions[i] += 1

    for i, accuracy in enumerate(correct_predictions):
        accuracy_percentage = accuracy / total_samples
        print(f"Model {i + 1} Accuracy on the folder '{folder_name}': {accuracy_percentage * 100:.2f}%")


model_paths = ['models_for_mgr/model 1A.h5', 'models_for_mgr/model 2A.h5', 'models_for_mgr/model 3A.h5',
             'models_for_mgr/model 1B.h5', 'models_for_mgr/model 2B.h5', 'models_for_mgr/model 3B.h5']

folder_path = 'objects_mini/R cartouche'

class_names = ["Steam engine", "L cartouche", "Lighthouse", "Church tower", "Logo PG", "Medusa", "Prow", "R cartouche"]

test_models_on_folder(model_paths, folder_path, class_names)
