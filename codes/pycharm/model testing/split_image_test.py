import cv2
import numpy as np
from keras.models import load_model
import os


def predict_object(model, image):
    img = cv2.resize(image, (360, 640))
    img = img.astype('float32') / 255.0
    img = np.expand_dims(img, axis=0)

    predictions = model.predict(img)

    predicted_class = np.argmax(predictions)

    return predicted_class, predictions[0][predicted_class]


def predict_object_splitted(model, image, predicted_class):
    img = cv2.resize(image, (360, 640))
    img = img.astype('float32') / 255.0
    img = np.expand_dims(img, axis=0)

    predictions = model.predict(img)

    predicted_classes = predicted_class

    return predicted_classes, predictions[0][predicted_classes]

def test_model_on_image(model, model2, image_path, class_names):

    original_image = cv2.imread(image_path)

    rows, cols, _ = original_image.shape

    predicted_class, confidence = predict_object(model, original_image)

    print(f"Predicted class for the whole image: {class_names[predicted_class]}, Confidence: {confidence * 100:.2f}%")

    step_size_row = rows // 3
    step_size_col = cols // 3

    best_result = None
    best_confidence = 0

    for i in range(3):
        for j in range(3):
            roi = original_image[i * step_size_row:(i + 1) * step_size_row,
                                 j * step_size_col:(j + 1) * step_size_col]

            # roi = cv2.resize(roi, (360, 640))

            _, confidence = predict_object_splitted(model2, roi, predicted_class)

            if confidence > best_confidence:
                best_result = (i, j, confidence)
                best_confidence = confidence

    if best_result is not None:
        i, j, confidence = best_result

        cv2.rectangle(original_image,
                      (j * step_size_col, i * step_size_row),
                      ((j + 1) * step_size_col, (i + 1) * step_size_row),
                      (0, 255, 0), 2)

        class_name = class_names[predicted_class]
        label_text = f"{class_name}: {confidence * 100:.0f}%"
        cv2.putText(original_image, label_text,
                    (j * step_size_col, i * step_size_row + 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

    cv2.imshow("Image with Rectangle", original_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


loaded_model = load_model('models_for_mgr/model 2A.h5')
loaded_model_2 = load_model('models_for_mgr/model 2B.h5')

class_names = ["Steam engine", "L cartouche", "Lighthouse", "Church tower", "Logo PG", "Medusa", "Prow", "R cartouche"]

folder_path = 'objects/test'


for filename in os.listdir(folder_path):
    if filename.endswith(".png") or filename.endswith(".jpg") or filename.endswith(".PNG"):

        image_path = os.path.join(folder_path, filename)

        test_model_on_image(loaded_model, loaded_model_2, image_path, class_names)