import cv2
import numpy as np
from keras.models import load_model


def preprocess_image(image):
    resized_image = cv2.resize(image, (360, 640))
    preprocessed_image = resized_image.astype('float32') / 255.0
    input_image = np.expand_dims(preprocessed_image, axis=0)
    return input_image


def predict_single_frame(model, frame, class_names):
    input_frame = preprocess_image(frame)
    predictions = model.predict(input_frame)
    predicted_class = np.argmax(predictions)
    class_name = class_names[predicted_class]
    confidence_percent = predictions[0][predicted_class] * 100
    return class_name, confidence_percent


def test_model_on_video(model_path, video_path, class_names):

    loaded_model = load_model(model_path)
    video_capture = cv2.VideoCapture(video_path)
    total_frames = int(video_capture.get(cv2.CAP_PROP_FRAME_COUNT))
    correct_predictions = 0

    while True:
        ret, frame = video_capture.read()

        if not ret:
            break

        predicted_class, confidence_percent = predict_single_frame(loaded_model, frame, class_names)

        print(f"Predicted class: {predicted_class}, Confidence: {confidence_percent:.2f}%")

        actual_class = video_path.split("/")[-1].split(".")[0]
        if predicted_class == actual_class:
            correct_predictions += 1

    video_capture.release()
    cv2.destroyAllWindows()

    accuracy = correct_predictions / total_frames * 100
    print(f"Accuracy on the video: {accuracy:.2f}%")

model_path = 'models_for_mgr/model 1A.h5'
video_path = 'videos/vid_1.mp4'
object_of_interest = "Lighthouse"


class_names = ["Steam engine", "L cartouche", "Lighthouse", "Church tower", "Logo PG", "Medusa", "Prow", "R cartouche"]

test_model_on_video(model_path, video_path, class_names)