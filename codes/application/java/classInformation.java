package com.mz.openvc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class classInformation {

    private static classInformation instance = null;
    private List<ObjectInfo> objectInfoList;

    private classInformation() {
        objectInfoList = new ArrayList<>();

        objectInfoList.add(new ObjectInfo("LEFT CARTOUCHE",
                "Po jednej stronie wschodniego kartusza widnieje orzeł (częściowo zniszczony), który stoi na dziobach pięciu łódek unoszących się na wodzie. Nad kolumnami były umieszczone przypuszczalnie latarnie morskie. Choć są one zniszczone, to na tej po prawej stronie widać, że ogon stwora tam umieszczonego zakończony jest płetwą, co sugeruje, że był on typu „morskiego”. Wynika z tego, że nad polem tekstowym znajdowały się dwa „stwory morskie”, między którymi umieszczony był inny element, symboliką nawiązujący do morza - tym elementem była muszla. Dziś zachował się jedynie dolny jej fragment, obok którego widać fale morskie.",
                R.drawable.left_cartouche_image));

        objectInfoList.add(new ObjectInfo("LIGHTHOUSE",
                "na rzeźbie widać latarnie morską, pod którą umieszczona jest głowa starca z długą brodą. Pod nim widać busolę, która wskazuje kierunek „północny, północny wschód”, a dokładniej azymut 30°, czyli kierunek ustawienia głównej osi całego założenia Politechniki. Pod busolą znajdują się dodatkowe elementy morskie – sznury i liny z bojami.",
                R.drawable.lighthouse_image));

        objectInfoList.add(new ObjectInfo("CHURCH TOWER",
                "Rzeźba przedstawia wieżę Katedry Mariackiej. Pod nią umieszczona jest twarz kobieca, ze skrzydłami i koroną w postaci muru (wieży). Niżej widnieje postać węża. Po dokładnym przyjrzeniu się widać, że zjada on własny ogon – takie przedstawienie nazywane jest „ouroboros” – jest on symbolem „kręgu życia”, i „nieskończoności”. Pod nim widnieje tarcza z herbem",
                R.drawable.church_tower_image));

        objectInfoList.add(new ObjectInfo("LOGO PG",
                "W centrum kartuszu umieszczony był niegdyś wizerunek cesarza Wilhelma II. Po wojnie został zastąpiony blaszanym wizerunkiem żaglowca w okręgu koła zębatego, dzisiaj znajduje się tam herb Gdańska wraz z inicjałami Politechniki Gdańskiej. Szczyt kartuszu wieńczyła niegdyś korona na poduszce, również zdemontowana po wojnie. Po jej  bokach znajdują się dwie głowy lwów",
                R.drawable.logo_pg_image));

        objectInfoList.add(new ObjectInfo("MEDUSA",
                "Rzeźba przedstawia głowę Meduzy, umieszczonej w kartuszu okraszonym dwiema pochodniami oraz muszlą. Po jej bokach siedzą dwa bobasy – jeden trzyma pion, drugi zaś zwój papieru, na którym widnieje umieszczony rzut Gmachu Głównego.",
                R.drawable.medusa_image));

        objectInfoList.add(new ObjectInfo("PROW",
                "Szczytem rzeźby jest dziób statku, wykonanego z drewna, zakończonego głową orła. Z dziobu statku zwisają swobodnie latarenki i bojki. Poniżej znajduje się postać symbolizująca wodę – jakby skrzyżowanie głowy ryby i ptaka, z elementami przypominającymi płetwy. Pod tą postacią znajduje się symbol muszli – bardzo często występującej zarówno na Politechnice, jak i w ogóle w Gdańsku – jest to symbol związany z morzem. Poniżej znajduje się kotwica zawieszona na łańcuchach, widocznych na całej rzeźbie. Pod nią widoczne są rośliny, przypuszczalnie rodzajwodorostów.",
                R.drawable.prow_image));

        objectInfoList.add(new ObjectInfo("RIGHT CARTOUCHE",
                "W kartuszu po zachodniej stronie, majestatyczny orzeł z rozpostartymi skrzydłami stoi na elemencie wyglądającym jak jakaś murowana rotunda. Podobne elementy, wyglądające jak wieże murowane z kamienia, umieszczone są nad obiema kolumnami. Oplatają je swoimi ogonami postacie dwóch „stworów ziemskich” – cechuje je normalnie zakończony ogon, oraz palczaste kończyny. Między nimi znajduje się element, składający się z wielościanów, wyglądających jak kryształy górskie.",
                R.drawable.right_cartouche_image));

        objectInfoList.add(new ObjectInfo("STEAM ENGINE",
                "Rzeźba jest częściowo zniszczona. Jednakże z zachowanych elementów można zauważyć, iż jej głównym elementem miała być okazała maszyna parowa. Poniżej przypuszczalnie znajdowała się postać, która jej towarzyszyła. Jedynie z kłębów dymu można się domyślać, że mogła być to postać symbolizująca siłę pary wodnej. Mały detal znajdujący się poniżej również jest zniszczony. Na samym dole widać częściowo zniszczone „skrzydlate koło”, które również jest elementem, który występuje jeszcze w paru miejscach Gmachu Głównego. Jest to symbol „szybkiego transportu”, który można również zauważyć na szczycie dworca PKP\n" +
                        "Gdańsk Główny.",
                R.drawable.steam_engine_image));
    }

    public static classInformation getInstance() {
        if (instance == null) {
            instance = new classInformation();
        }
        return instance;
    }

    public List<ObjectInfo> getObjectInfoList() {
        return objectInfoList;
    }

    public class ObjectInfo {
        private String objectName;
        private String objectDescription;
        private int imageResId;

        public ObjectInfo(String objectName, String objectDescription, int imageResId) {
            this.objectName = objectName;
            this.objectDescription = objectDescription;
            this.imageResId = imageResId;
        }

        public String getObjectName() {
            return objectName;
        }

        public String getObjectDescription() {
            return objectDescription;
        }

        public int getImageResId() {
            return imageResId;
        }
    }
}