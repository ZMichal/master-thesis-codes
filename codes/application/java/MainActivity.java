package com.mz.openvc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;


import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.gpu.GpuDelegate;


public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private Mat lastProcessedImage;
    private List<objectDetectorClass.DetectedObject> detectedObjects;

    List<classInformation.ObjectInfo> objectInfoList;

    private Interpreter tflite;
    private GpuDelegate gpuDelegate = null;

    CameraBridgeViewBase cameraBridgeViewBase;
    BaseLoaderCallback baseLoaderCallback;

    private Mat mRgba;
    private Mat mIntermediateMat;
    private Mat mGray;
    private objectDetectorClass objectDetector;
    Mat hierarchy;
    List<MatOfPoint> contours;
    private int frameCount = 0;

    private Button storage_prediction;
    private Button objects_information;

    private TextView fpsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cameraBridgeViewBase = (JavaCameraView)findViewById(R.id.CameraView);
        cameraBridgeViewBase.setVisibility(View.VISIBLE);
        cameraBridgeViewBase.setCvCameraViewListener(this);

        objectInfoList = classInformation.getInstance().getObjectInfoList();

        fpsTextView = findViewById(R.id.fpsTextView);

        try {
            objectDetector = new objectDetectorClass(getAssets(), "custom_model.tflite","custom_label.txt", 320);
            objectDetector.setFrameCounter(frameCount);
            Log.d("MainActivity", "Model is successfully loaded");
        } catch (IOException e) {
            e.printStackTrace();
        }

        cameraBridgeViewBase.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Log.d("Touch", "Touch event detected");
                Log.d("ClickCoords", "getX: " +  event.getX() + " getY: " + event.getY());
                Log.d("ClickCoords", "width: " +  cameraBridgeViewBase.getWidth() + " height: " + cameraBridgeViewBase.getHeight());
                Log.d("ClickCoords", "cols: " +  lastProcessedImage.cols() + " rows: " +  lastProcessedImage.rows());

                float x = event.getX();
                float y = event.getY();

                float scaleFactorX = (float) lastProcessedImage.rows() / cameraBridgeViewBase.getWidth();
                float scaleFactorY = (float) lastProcessedImage.cols() / cameraBridgeViewBase.getHeight();

                int xInImage = (int) (x * scaleFactorX);
                int yInImage = (int) (y * scaleFactorY);

                Log.d("ClickCoords", "Screen X: " + xInImage + " Y: " + yInImage);

                for (objectDetectorClass.DetectedObject detectedObject : detectedObjects) {
                    int left = (int) (detectedObject.getLeft());
                    int right = (int) (detectedObject.getRight());
                    int top = (int) (detectedObject.getTop());
                    int bottom = (int) (detectedObject.getBottom());


                    Log.d("rectangle", "left: " + left + " right: " + right + " top: " + top + " bottom: " + bottom );
                    String className = detectedObject.getClassName();

                    int margin = 1;

                    if (xInImage >= left - margin && xInImage <= right + margin &&
                            yInImage >= top - margin && yInImage <= bottom + margin) {
                        Log.d("ObjectClick", "Clicked on object: " + className);
                        if ("L CARTOUCHE".equals(className)) {
                            Log.d("ObjectClick", "Launching L CARTOUCHE info activity");
                            Intent intent = new Intent(MainActivity.this, ObjectInfoActivity.class);
                            intent.putExtra("objectName", objectInfoList.get(0).getObjectName());
                            intent.putExtra("objectDescription", objectInfoList.get(0).getObjectDescription());
                            intent.putExtra("imageResId", objectInfoList.get(0).getImageResId());  startActivity(intent);
                        }
                        else if ("LIGHTHOUSE".equals(className)) {
                            Intent intent = new Intent(MainActivity.this, ObjectInfoActivity.class);
                            intent.putExtra("objectName", objectInfoList.get(1).getObjectName());
                            intent.putExtra("objectDescription", objectInfoList.get(1).getObjectDescription());
                            intent.putExtra("imageResId", objectInfoList.get(1).getImageResId()); startActivity(intent);
                        }
                        else if ("CHURCH TOWER".equals(className)) {
                            Intent intent = new Intent(MainActivity.this, ObjectInfoActivity.class);
                            intent.putExtra("objectName", objectInfoList.get(2).getObjectName());
                            intent.putExtra("objectDescription", objectInfoList.get(2).getObjectDescription());
                            intent.putExtra("imageResId", objectInfoList.get(2).getImageResId()); startActivity(intent);
                        }
                        else if ("LOGO PG".equals(className)) {
                            Intent intent = new Intent(MainActivity.this, ObjectInfoActivity.class);
                            intent.putExtra("objectName", objectInfoList.get(3).getObjectName());
                            intent.putExtra("objectDescription", objectInfoList.get(3).getObjectDescription());
                            intent.putExtra("imageResId", objectInfoList.get(3).getImageResId()); startActivity(intent);
                        }
                        else if ("MEDUSA".equals(className)) {
                            Intent intent = new Intent(MainActivity.this, ObjectInfoActivity.class);
                            intent.putExtra("objectName", objectInfoList.get(4).getObjectName());
                            intent.putExtra("objectDescription", objectInfoList.get(4).getObjectDescription());
                            intent.putExtra("imageResId", objectInfoList.get(4).getImageResId()); startActivity(intent);
                        }
                        else if ("PROW".equals(className)) {
                            Intent intent = new Intent(MainActivity.this, ObjectInfoActivity.class);
                            intent.putExtra("objectName", objectInfoList.get(5).getObjectName());
                            intent.putExtra("objectDescription", objectInfoList.get(5).getObjectDescription());
                            intent.putExtra("imageResId", objectInfoList.get(5).getImageResId()); startActivity(intent);
                        }
                        else if ("R CARTOUCHE".equals(className)) {
                            Intent intent = new Intent(MainActivity.this, ObjectInfoActivity.class);
                            intent.putExtra("objectName", objectInfoList.get(6).getObjectName());
                            intent.putExtra("objectDescription", objectInfoList.get(6).getObjectDescription());
                            intent.putExtra("imageResId", objectInfoList.get(6).getImageResId()); startActivity(intent);
                        }
                        else if ("STEAM ENGINE".equals(className)) {
                            Intent intent = new Intent(MainActivity.this, ObjectInfoActivity.class);
                            intent.putExtra("objectName", objectInfoList.get(7).getObjectName());
                            intent.putExtra("objectDescription", objectInfoList.get(7).getObjectDescription());
                            intent.putExtra("imageResId", objectInfoList.get(7).getImageResId()); startActivity(intent);
                        }

                        break;
                    }
                }
            }
            return true;
        });



        baseLoaderCallback = new BaseLoaderCallback(this) {
            @Override
            public void onManagerConnected(int status) {
                super.onManagerConnected(status);
                switch (status){
                    case BaseLoaderCallback.SUCCESS:
                        cameraBridgeViewBase.enableView();
                        break;
                    default:
                        super.onManagerConnected(status);
                        break;
                }
            }
        };

        storage_prediction=findViewById(R.id.storage_prediction);
        storage_prediction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,StoragePredictionActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });

        objects_information=findViewById(R.id.objects_information);
        objects_information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,ObjectsInformationActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });
    }

    private Interpreter.Options getInterpreterOptions() {
        Interpreter.Options options = new Interpreter.Options();
        gpuDelegate = new GpuDelegate();
        options.addDelegate(gpuDelegate);
        return options;
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat();
        mIntermediateMat = new Mat();
        mGray = new Mat();
        hierarchy = new Mat();
    }

    @Override
    public void onCameraViewStopped() {
        mRgba.release();
        mGray.release();
        mIntermediateMat.release();
        hierarchy.release();

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();

        long startTime = System.currentTimeMillis();

        Mat result = objectDetector.recognizeImage(mRgba);

        lastProcessedImage = result;
        detectedObjects = objectDetector.getDetectedObjects();

        frameCount++;

        if (frameCount % 32 == 0) {
            objectDetector.setFrameCounter(frameCount);
            detectedObjects.clear();
        }

        long endTime = System.currentTimeMillis();

        double fps = 1000.0 / (endTime - startTime);

        runOnUiThread(() -> fpsTextView.setText(String.format("FPS: %.0f", fps)));


        return result;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()){
            Toast.makeText(this, "There is some problem", Toast.LENGTH_SHORT).show();
        } else {
            baseLoaderCallback.onManagerConnected(baseLoaderCallback.SUCCESS);
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (!OpenCVLoader.initDebug()){
            Toast.makeText(this, "There is some problem", Toast.LENGTH_SHORT).show();
        }else {
            baseLoaderCallback.onManagerConnected(baseLoaderCallback.SUCCESS);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(cameraBridgeViewBase != null){
            cameraBridgeViewBase.disableView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (cameraBridgeViewBase != null){
            cameraBridgeViewBase.disableView();
        }
    }
}