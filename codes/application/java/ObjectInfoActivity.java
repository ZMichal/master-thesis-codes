package com.mz.openvc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ObjectInfoActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView objectNameTextView;
    private TextView objectDescriptionTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_info);

        imageView = findViewById(R.id.imageView);
        objectNameTextView = findViewById(R.id.objectNameTextView);
        objectDescriptionTextView = findViewById(R.id.objectDescriptionTextView);

        Intent intent = getIntent();
        if (intent != null) {
            String objectName = intent.getStringExtra("objectName");
            String objectDescription = intent.getStringExtra("objectDescription");
            int imageResId = intent.getIntExtra("imageResId", 0);

            objectNameTextView.setText(objectName);
            objectDescriptionTextView.setText(objectDescription);
            imageView.setImageResource(imageResId);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}