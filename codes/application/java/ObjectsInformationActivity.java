package com.mz.openvc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;



import java.util.List;

public class ObjectsInformationActivity extends AppCompatActivity {

    List<classInformation.ObjectInfo> objectInfoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_objects_information);

        Button buttonObject1 = findViewById(R.id.buttonObject1);
        Button buttonObject2 = findViewById(R.id.buttonObject2);
        Button buttonObject3 = findViewById(R.id.buttonObject3);
        Button buttonObject4 = findViewById(R.id.buttonObject4);
        Button buttonObject5 = findViewById(R.id.buttonObject5);
        Button buttonObject6 = findViewById(R.id.buttonObject6);
        Button buttonObject7 = findViewById(R.id.buttonObject7);
        Button buttonObject8 = findViewById(R.id.buttonObject8);

        objectInfoList = classInformation.getInstance().getObjectInfoList();

        // LEFT CARTOUCHE
        buttonObject1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ObjectsInformationActivity.this, ObjectInfoActivity.class);
                intent.putExtra("objectName", objectInfoList.get(0).getObjectName());
                intent.putExtra("objectDescription", objectInfoList.get(0).getObjectDescription());
                intent.putExtra("imageResId", objectInfoList.get(0).getImageResId());
                startActivity(intent);
            }
        });

        // LIGHTHOUSE
        buttonObject2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ObjectsInformationActivity.this, ObjectInfoActivity.class);
                intent.putExtra("objectName", objectInfoList.get(1).getObjectName());
                intent.putExtra("objectDescription", objectInfoList.get(1).getObjectDescription());
                intent.putExtra("imageResId", objectInfoList.get(1).getImageResId());
                startActivity(intent);
            }
        });

        // CHURCH TOWER
        buttonObject3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ObjectsInformationActivity.this, ObjectInfoActivity.class);
                intent.putExtra("objectName", objectInfoList.get(2).getObjectName());
                intent.putExtra("objectDescription", objectInfoList.get(2).getObjectDescription());
                intent.putExtra("imageResId", objectInfoList.get(2).getImageResId());
                startActivity(intent);
            }
        });

        // LOGO PG
        buttonObject4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ObjectsInformationActivity.this, ObjectInfoActivity.class);
                intent.putExtra("objectName", objectInfoList.get(3).getObjectName());
                intent.putExtra("objectDescription", objectInfoList.get(3).getObjectDescription());
                intent.putExtra("imageResId", objectInfoList.get(3).getImageResId());
                startActivity(intent);
            }
        });

        // MEDUSA
        buttonObject5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ObjectsInformationActivity.this, ObjectInfoActivity.class);
                intent.putExtra("objectName", objectInfoList.get(4).getObjectName());
                intent.putExtra("objectDescription", objectInfoList.get(4).getObjectDescription());
                intent.putExtra("imageResId", objectInfoList.get(4).getImageResId());
                startActivity(intent);
            }
        });

        // PROW
        buttonObject6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ObjectsInformationActivity.this, ObjectInfoActivity.class);
                intent.putExtra("objectName", objectInfoList.get(5).getObjectName());
                intent.putExtra("objectDescription", objectInfoList.get(5).getObjectDescription());
                intent.putExtra("imageResId", objectInfoList.get(5).getImageResId());
                startActivity(intent);
            }
        });

        // RIGHT CARTOUCHE
        buttonObject7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ObjectsInformationActivity.this, ObjectInfoActivity.class);
                intent.putExtra("objectName", objectInfoList.get(6).getObjectName());
                intent.putExtra("objectDescription", objectInfoList.get(6).getObjectDescription());
                intent.putExtra("imageResId", objectInfoList.get(6).getImageResId()); startActivity(intent);
            }
        });

        // STEAM ENGINE
        buttonObject8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ObjectsInformationActivity.this, ObjectInfoActivity.class);
                intent.putExtra("objectName", objectInfoList.get(7).getObjectName());
                intent.putExtra("objectDescription", objectInfoList.get(7).getObjectDescription());
                intent.putExtra("imageResId", objectInfoList.get(7).getImageResId()); startActivity(intent);
            }
        });

    }
}