package com.mz.openvc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.IOException;
import java.util.List;

public class StoragePredictionActivity extends AppCompatActivity {

    private Button select_image;
    private ImageView image_v;
    private objectDetectorClass objectDetectorClass;
    int SELECT_PICTURE=200;

    private Mat lastProcessedImage;
    private List<objectDetectorClass.DetectedObject> detectedObjects;

    private long lastClickTime = 0;
    private static final long CLICK_DELAY = 500;

    List<classInformation.ObjectInfo> objectInfoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage_prediction);

        select_image=findViewById(R.id.select_image);
        image_v=findViewById(R.id.image_v);

        objectInfoList = classInformation.getInstance().getObjectInfoList();

        //load model
        try{
            objectDetectorClass=new objectDetectorClass(getAssets(),"custom8.tflite","custom8_label.txt",320);
            Log.d("MainActivity","Model is successfully loaded");
        }
        catch (IOException e){
            Log.d("MainActivity","Getting some error");
            e.printStackTrace();
        }

        image_v.setOnTouchListener((v, event) -> {
            if (detectedObjects != null && lastProcessedImage != null) {
                int x = (int) (event.getX() * lastProcessedImage.cols() / v.getWidth());
                int y = (int) (event.getY() * lastProcessedImage.rows() / v.getHeight());
                Log.d("ClickCoords", "getX: " +  event.getX() + " getY: " + event.getY());
                Log.d("ClickCoords", "width: " +  v.getWidth() + " height: " + v.getHeight());
                Log.d("ClickCoords", "cols: " +  lastProcessedImage.cols() + " rows: " +  lastProcessedImage.rows());
                Log.d("ClickCoords", "Clicked at X: " + x + " Y: " + y);
                long currentTime = System.currentTimeMillis();
                if (currentTime - lastClickTime > CLICK_DELAY) {
                    lastClickTime = currentTime;
                    for (objectDetectorClass.DetectedObject detectedObject : detectedObjects) {
                        int left = (int) (detectedObject.getLeft());
                        int right = (int) (detectedObject.getRight());
                        int top = (int) (detectedObject.getTop());
                        int bottom = (int) (detectedObject.getBottom());
                        Log.d("rectangle", "left" + left + "right" + right + "top" + top + "bottom " + bottom );


                        String className = detectedObject.getClassName();

                        if (x >= left && x <= right && y >= top && y <= bottom) {

                            if ("L CARTOUCHE".equals(className)) {
                                Intent intent = new Intent(StoragePredictionActivity.this, ObjectInfoActivity.class);
                                intent.putExtra("objectName", objectInfoList.get(0).getObjectName());
                                intent.putExtra("objectDescription", objectInfoList.get(0).getObjectDescription());
                                intent.putExtra("imageResId", objectInfoList.get(0).getImageResId());
                                startActivity(intent);
                            } else if ("LIGHTHOUSE".equals(className)) {
                                Intent intent = new Intent(StoragePredictionActivity.this, ObjectInfoActivity.class);
                                intent.putExtra("objectName", objectInfoList.get(1).getObjectName());
                                intent.putExtra("objectDescription", objectInfoList.get(1).getObjectDescription());
                                intent.putExtra("imageResId", objectInfoList.get(1).getImageResId());
                                startActivity(intent);
                            } else if ("CHURCH TOWER".equals(className)) {
                                Intent intent = new Intent(StoragePredictionActivity.this, ObjectInfoActivity.class);
                                intent.putExtra("objectName", objectInfoList.get(2).getObjectName());
                                intent.putExtra("objectDescription", objectInfoList.get(2).getObjectDescription());
                                intent.putExtra("imageResId", objectInfoList.get(2).getImageResId());
                                startActivity(intent);
                            } else if ("LOGO PG".equals(className)) {
                                Intent intent = new Intent(StoragePredictionActivity.this, ObjectInfoActivity.class);
                                intent.putExtra("objectName", objectInfoList.get(3).getObjectName());
                                intent.putExtra("objectDescription", objectInfoList.get(3).getObjectDescription());
                                intent.putExtra("imageResId", objectInfoList.get(3).getImageResId());
                                startActivity(intent);
                            } else if ("MEDUSA".equals(className)) {
                                Intent intent = new Intent(StoragePredictionActivity.this, ObjectInfoActivity.class);
                                intent.putExtra("objectName", objectInfoList.get(4).getObjectName());
                                intent.putExtra("objectDescription", objectInfoList.get(4).getObjectDescription());
                                intent.putExtra("imageResId", objectInfoList.get(4).getImageResId());
                                startActivity(intent);
                            } else if ("PROW".equals(className)) {
                                Intent intent = new Intent(StoragePredictionActivity.this, ObjectInfoActivity.class);
                                intent.putExtra("objectName", objectInfoList.get(5).getObjectName());
                                intent.putExtra("objectDescription", objectInfoList.get(5).getObjectDescription());
                                intent.putExtra("imageResId", objectInfoList.get(5).getImageResId());
                                startActivity(intent);
                            } else if ("R CARTOUCHE".equals(className)) {
                                Intent intent = new Intent(StoragePredictionActivity.this, ObjectInfoActivity.class);
                                intent.putExtra("objectName", objectInfoList.get(6).getObjectName());
                                intent.putExtra("objectDescription", objectInfoList.get(6).getObjectDescription());
                                intent.putExtra("imageResId", objectInfoList.get(6).getImageResId());
                                startActivity(intent);
                            }
                            else if ("STEAM ENGINE".equals(className)) {
                                Intent intent = new Intent(StoragePredictionActivity.this, ObjectInfoActivity.class);
                                intent.putExtra("objectName", objectInfoList.get(7).getObjectName());
                                intent.putExtra("objectDescription", objectInfoList.get(7).getObjectDescription());
                                intent.putExtra("imageResId", objectInfoList.get(7).getImageResId()); startActivity(intent);
                            }
                            break;
                        }
                    }
                }
            }
            return true;
        });

        select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detectedObjects != null) {
                    detectedObjects.clear();
                }
                image_chooser();
            }
        });
    }

    private void image_chooser() {
        Intent i =new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_PICTURE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode==RESULT_OK) {
            if(requestCode==SELECT_PICTURE){
                Uri selectedImageUri=data.getData();
                if(selectedImageUri !=null){
                    Log.d("StoragePrediction", "Output_uri: "+selectedImageUri);
                    Bitmap bitmap=null;
                    try{
                        bitmap= MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImageUri);
                    }
                    catch (IOException e){
                        e.printStackTrace();
                    }
                    assert bitmap != null;
                    Mat selected_image =new Mat(bitmap.getHeight(),bitmap.getWidth(), CvType.CV_8UC4);
                    Utils.bitmapToMat(bitmap,selected_image);

                    lastProcessedImage = objectDetectorClass.recognizePhoto(selected_image);
                    detectedObjects = objectDetectorClass.getDetectedObjects();

                    Bitmap bitmap1=null;
                    bitmap1=Bitmap.createBitmap(selected_image.cols(),selected_image.rows(),Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(selected_image,bitmap1);

                    image_v.setImageBitmap(bitmap1);
                }
            }
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}